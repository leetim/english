# Passive Active

## I

1. **Во время экскурсии по городу нам показали завод, где производится сталь.** - During a city tour we were shown a factory
2. **Со мной так никогда еще не разговаривали.** - I have never been talked such way yet.
3. **Когда мы приехали в Киев, это здание было только что восстановлено.** - When we arrived to Kiev, this building was just repaired
4. **В прощлом месяце, наконец, вышла новая книга. Сейчас её широко обсуждают в прессе.** - Finally new book was released last month. Now it is discused in the press widely.
5. **Не говори таких вещей, а то над тобой будут смеяться.** - Don't talk such things or you will be laughed at.
6. **Мне еще ничего об этом не говорили.** - I haven't been talked about it yet.
7. 
    - **Ваш проект уже принят?** - Have your project been accepted already.
    - **Нет он все еще рассматривается.** - No, it havn't. It's still being considered.
    - **Сколько же времени его уже рассматривают?** - How much time it has been considered.
8. **Вчера ему предложили новую работу, а она ему не нужна.** - He was offered a new job, but it isn't needed for him.
9. **Я не знал кому меня представляют.** - I didn't know whom i was introduced to.
10. **На приеме на профессора не обратили внимания, а вокруг его хорошенькой жены развели суету (make fuss of)** - At the reseption Professor wasn't paid attention at, but there was fuzzing off around his pretty wife.

## II

1. **Что-нибудь делается что бы восстановить это здание?** - Is there something to do that building to be restored.
2. **Его еще никогда не принимали за англичанина.** - he hasn't ever mistaken for Englishman yet.
3. **За каждый цент нужно отчитаться (account for)** - Every cent is needed to account for.
4. **Вас когда-нибудь учили как надо вести себя?** - have you ever been teached how to behave yourself.
5. **Детей угостили (treat to) мороженным.** - Children were treat to the icecream.
6. **У меня украли коллекцию марок.** - The collection of marks was stolen from me.
7. **Надо что-то сделать для этих людей.** - These people are needed to do something for themselves.
8. **Боюсь что эту вазу нельзя починить.** - I'm afraid, this vase will not ever been to repair.
9. **О его приятеле хорошо отзываются** - His buddy speaks well.
10. **Моего дядю произвели (promote to) в капитаны.** - My uncle was promoted to captain.
11. **Тебе скажут, когда отправляется поезд.** - You will be told when train leaves.
12. **Будет так темно, что меня совсем не будет видно.** - It will be so dark that I will not be visible at all.
13. 
    - **Почему так прохладно в зале?** - why there is so cold in the hall.
    - **Его как раз проветривают (air)? Читальный зал как вы знаете проветривается несколько раз в день.** - it is just aired. As you know reading hall is been aired several times a day.

## III

1. **Больного не будут оперировать без его согласия.** Sick will be not operated without his agreement.
2. **Телеграмма была принята поздней ночью, и, так как она была очень важной, капитана тут же разбудили.** - Telegram was accepted at late night, and capitan woke up beacause it was vety important.
3. **Факты, на которые ссылается свидетель, заинтерисовали адвоката.** - Facts that was referred by witness interested lawyer.
4. **Проект был в основном одобрен, но архитектуру указали на отдельные недостатки.** - Project was basicaly approved, but architect pointed out certain flaws.
5. **Советую вам пойти на этот концерт: будут исполнены ваши любимые песни.** - I advice you to go at this concert: your favorite song will be performed.
6. **Нам объяснили новое правило, затем продиктовали несколько примеров.** - A new rule had been explained for us, then few examples was dictated.
7. **Не беспокойтесь, о вашем багаже позаботятся и он будет доставлен в номер.** - Do not worry, your luggage will be taken care of and it will be delivered to your room.
8. **Он не слышал, что в это время говорилось.** - He didn't hear what was being said at this time.
9. **Мы узнаем, хорошо ли за ним смотрели.** - we will find out, if he was watched well.
10. **Ему дали первоклассное образование.**  - he was given first-class education.
